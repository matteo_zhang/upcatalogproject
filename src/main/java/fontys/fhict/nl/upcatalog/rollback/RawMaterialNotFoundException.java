package fontys.fhict.nl.upcatalog.rollback;

public class RawMaterialNotFoundException extends RuntimeException {

    public RawMaterialNotFoundException(Long id) {
        super("Could not find employee " + id);
    }
}
