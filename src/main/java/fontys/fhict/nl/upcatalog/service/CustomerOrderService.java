package fontys.fhict.nl.upcatalog.service;

import fontys.fhict.nl.upcatalog.dto.CartDTO;
import fontys.fhict.nl.upcatalog.dto.OrderDTO;
import fontys.fhict.nl.upcatalog.dto.ProductCartDTO;
import fontys.fhict.nl.upcatalog.model.CustomerOrder;
import fontys.fhict.nl.upcatalog.model.cart.Cart;
import fontys.fhict.nl.upcatalog.model.cart.ProductCart;
import fontys.fhict.nl.upcatalog.repository.CustomerOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class CustomerOrderService {
    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    @Autowired
    private CartService cartService;

    public List<OrderDTO> getOrderList() {
        // return OrderDTO which contains the product list
        List<OrderDTO> order = new ArrayList<>();

        // get all customer user orders
        List<CustomerOrder> customerOrderList = this.customerOrderRepository.findAll();

        for(CustomerOrder item: customerOrderList) {
            Set<ProductCartDTO> items = new HashSet<>();

            List<CartDTO> cart = cartService.getEnabledCartList(item.getCustomer().getId());

            for(CartDTO cartItem : cart) {
                items.add(new ProductCartDTO(cartItem.getQuantity(), cartItem.getProduct()));
            }

            order.add(new OrderDTO(item.getCart(), item.getCustomer(), items, this.calculateTotalAmountDTO(items)));
        }

        return order;
    }

    public List<OrderDTO> getOrderListByCustomerId(Long customerId) {
        // return OrderDTO which contains the product list
        List<OrderDTO> order = new ArrayList<>();

        // get all customer user orders
        List<CustomerOrder> customerOrderList = this.customerOrderRepository.findAllByCustomerUserId(customerId);

        for(CustomerOrder item: customerOrderList) {
            Set<ProductCartDTO> items = new HashSet<>();

            List<CartDTO> cart = cartService.getEnabledCartList(customerId);

            for(CartDTO cartItem : cart) {
                items.add(new ProductCartDTO(cartItem.getQuantity(), cartItem.getProduct()));
            }

            order.add(new OrderDTO(item.getCart(), item.getCustomer(), items, this.calculateTotalAmountDTO(items)));
        }

        return order;
    }

    public ResponseEntity generateOrder(Long customerId) {
        // get which cart the customer user is using
        List<CartDTO> cart = this.cartService.getEnabledCartList(customerId);

        Set<ProductCart> items = new HashSet<>();

        // leave cart as null since there is no need to know it.
        for(CartDTO item : cart) {
            items.add(new ProductCart(item.getProduct(), null, item.getQuantity()));
        }

        double totalAmount = this.calculateTotalAmount(items);
        // get cart instance
        Cart foundCart = cartService.getEnabledCart(customerId);

        CustomerOrder customerOrder = new CustomerOrder(foundCart, cart.get(0).getCustomerUser(), totalAmount);

        // save the order
        this.customerOrderRepository.save(customerOrder);

        // disable currently using cart
        if(this.cartService.disableCurrentlyUsingCart(customerId))
            return new ResponseEntity<>(HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private double calculateTotalAmountDTO(Set<ProductCartDTO> items) {
        double totalAmount = 0;

        for(ProductCartDTO item : items) {
            // important: an instance of an item is made of stocks.
            // E.g. 1 item of iron could be 300 piece of iron; you cannot buy for instance 1 piece of iron
            // this is an application for B2B, and not B2C.

            // item.getQuantity() indicate how many stocks the customer wants to purchase
            totalAmount += item.getProduct().calculatePricePerStock() * item.getQuantity();
        }

        return totalAmount;
    }

    private double calculateTotalAmount(Set<ProductCart> items) {
        double totalAmount = 0;

        for(ProductCart item : items) {
            // important: an instance of an item is made of stocks.
            // E.g. 1 item of iron could be 300 piece of iron; you cannot buy for instance 1 piece of iron
            // this is an application for B2B, and not B2C.

            // item.getQuantity() indicate how many stocks the customer wants to purchase
            totalAmount += item.getProduct().calculatePricePerStock() * item.getQuantity();
        }

        return totalAmount;
    }
}
