package fontys.fhict.nl.upcatalog.service;

import fontys.fhict.nl.upcatalog.model.item.Supplier;
import fontys.fhict.nl.upcatalog.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplierService {
    @Autowired
    SupplierRepository supplierRepository;

    public List<Supplier> getAllSupplierList() {
        return supplierRepository.findAll();
    }

}
