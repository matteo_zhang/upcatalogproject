package fontys.fhict.nl.upcatalog.service;

import fontys.fhict.nl.upcatalog.dto.CustomerNotificationDTO;
import fontys.fhict.nl.upcatalog.model.notification.CustomerNotification;
import fontys.fhict.nl.upcatalog.model.notification.NotificationType;
import fontys.fhict.nl.upcatalog.repository.CustomerNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerNotificationService {
    @Autowired
    private CustomerNotificationRepository customerNotificationRepository;

    public List<CustomerNotification> getAllCustomerNotification() {
        return this.customerNotificationRepository.findAll();
    }

    public CustomerNotification addCustomerNotification(CustomerNotificationDTO customerNotification) {
        String message = "Request customer account for UpCatalog system";

        CustomerNotification newCustomerNotification = new CustomerNotification(
                message,
                NotificationType.NewCustomerAccountRequest,
                customerNotification.getCustomerName(),
                customerNotification.getCustomerSurname(),
                customerNotification.getUsername(),
                customerNotification.getPassword(),
                customerNotification.getCustomerEmail(),
                customerNotification.getCompanyName(),
                customerNotification.getVATNumber(),
                customerNotification.getAddress(),
                customerNotification.getTelephoneNumber(),
                customerNotification.getCompanyEmail()
        );

        return this.customerNotificationRepository.save(newCustomerNotification);
    }
}
