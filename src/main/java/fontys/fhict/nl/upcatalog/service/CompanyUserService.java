package fontys.fhict.nl.upcatalog.service;

import fontys.fhict.nl.upcatalog.model.user.CompanyUser;
import fontys.fhict.nl.upcatalog.model.user.Role;
import fontys.fhict.nl.upcatalog.payload.response.JwtResponse;
import fontys.fhict.nl.upcatalog.payload.response.MessageResponse;
import fontys.fhict.nl.upcatalog.repository.CompanyUserRepository;
import fontys.fhict.nl.upcatalog.repository.RoleRepository;
import fontys.fhict.nl.upcatalog.security.JWT.JwtUtils;
import fontys.fhict.nl.upcatalog.service.userDetails.CompanyUserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CompanyUserService {
    @Autowired
    private CompanyUserRepository companyUserRepository;

//    @Autowired
//    private RoleRepository roleRepository;
//
//    @Autowired
//    private AuthenticationManager authenticationManager;
//
//    @Autowired
//    private JwtUtils jwtUtils;
//
//    @Autowired
//    private BCryptPasswordEncoder encoder;

    // TODO test it
    public List<CompanyUser> getAllCompanyUsers() {
        return this.companyUserRepository.findAll();
//
//        // get company user role
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN"))) {
//            return Stream.of(new CompanyUser(), new CompanyUser(), new CompanyUser()).collect(Collectors.toList());
//        }
//        else
//            return Stream.of(new CompanyUser(), new CompanyUser()).collect(Collectors.toList());
    }

    public Optional<CompanyUser> findCompanyUserById(Long id) {
        return this.companyUserRepository.findById(id);
    }

    // todo update method
    public CompanyUser updateCompanyUsersById(CompanyUser user) {
        return this.companyUserRepository.save(user);
    }

    // TOdo disable method

}
