//package fontys.fhict.nl.upcatalog.service.userDetails;
//
//import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
//import fontys.fhict.nl.upcatalog.repository.CustomerUserRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//@Service
//public class CustomerUserDetailsService implements UserDetailsService {
//    @Autowired
//    CustomerUserRepository customerUserRepository;
//
//    @Override
//    @Transactional
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        // find customer user
//        System.out.println("customer user service");
//        CustomerUser user = customerUserRepository.findByUsername(username)
//                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
//
//        // return customer user with authorities
//        return CustomerUserDetailsImpl.build(user);
//    }
//
//
//}
//
