package fontys.fhict.nl.upcatalog.service;

import fontys.fhict.nl.upcatalog.model.item.RawMaterial;
import fontys.fhict.nl.upcatalog.repository.RawMaterialRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RawMaterialService {
    @Autowired
    private RawMaterialRepository rawMaterialRepository;

    public List<RawMaterial> getAllRawMaterials() {
        return rawMaterialRepository.findAll();
    }

    public Optional<RawMaterial> findRawMaterialById(Long id) {
        return this.rawMaterialRepository.findById(id);
    }

    public RawMaterial addRawMaterial(RawMaterial rawMaterial) {
        LocalDateTime now = LocalDateTime.now();
        rawMaterial.setImagePath("no_image");
        rawMaterial.setRemoved(false);
        return rawMaterialRepository.save(rawMaterial);
    }

    public ResponseEntity<?> updateRawMaterial(RawMaterial rawMaterial, Long id) {
        try {
            Optional<RawMaterial> foundRawMaterial = this.rawMaterialRepository.findById(id);

            if(foundRawMaterial.isPresent()) {
                this.rawMaterialRepository.save(rawMaterial);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            else
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    // sets isRemoved field in true
    public ResponseEntity.BodyBuilder removeRawMaterial(Long id) {
        try {
            Optional<RawMaterial> foundRawMaterial = this.rawMaterialRepository.findById(id);

            if(foundRawMaterial.isPresent()) {
                RawMaterial rawMaterial = foundRawMaterial.get();
                rawMaterial.setRemoved(true);
                this.rawMaterialRepository.save(rawMaterial);

                return ResponseEntity.ok() ;
            }
            else
                return ResponseEntity.status(HttpStatus.NOT_FOUND);
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST);
        }
    }

    // deletes a row in database, must have isRemoved field as true
    public ResponseEntity.BodyBuilder deleteRawMaterial(Long id) {
        try {
            Optional<RawMaterial> foundRawMaterial = this.rawMaterialRepository.findById(id);

            if(foundRawMaterial.isPresent()) {
                RawMaterial rawMaterial = foundRawMaterial.get();

                // only previously removed item can be eventually deleted from database
                if(rawMaterial.isRemoved()) {
                    this.rawMaterialRepository.delete(rawMaterial);

                    return ResponseEntity.ok() ;
                }
                else
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST);
            }
            else
                return ResponseEntity.status(HttpStatus.NOT_FOUND);
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST);
        }
    }
}
