package fontys.fhict.nl.upcatalog.service;

import fontys.fhict.nl.upcatalog.dto.auth.CompanyUserSignUpRequestDTO;
import fontys.fhict.nl.upcatalog.dto.auth.CustomerUserSignUpRequestDTO;
import fontys.fhict.nl.upcatalog.dto.auth.UserSignInRequestDTO;
import fontys.fhict.nl.upcatalog.model.user.CompanyUser;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import fontys.fhict.nl.upcatalog.model.user.Role;
import fontys.fhict.nl.upcatalog.model.user.RoleName;
import fontys.fhict.nl.upcatalog.payload.response.JwtResponse;
import fontys.fhict.nl.upcatalog.payload.response.MessageResponse;
import fontys.fhict.nl.upcatalog.repository.CompanyUserRepository;
import fontys.fhict.nl.upcatalog.repository.CustomerUserRepository;
import fontys.fhict.nl.upcatalog.repository.RoleRepository;
import fontys.fhict.nl.upcatalog.security.JWT.JwtUtils;
import fontys.fhict.nl.upcatalog.service.userDetails.CompanyUserDetailsImpl;
import fontys.fhict.nl.upcatalog.service.userDetails.CustomerUserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UpCatalogUserService {
    @Autowired
    private CustomerUserRepository customerUserRepository;

    @Autowired
    private CompanyUserRepository companyUserRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public ResponseEntity authenticateUser(UserSignInRequestDTO loginCustomerUser) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginCustomerUser.getUsername(),
                        loginCustomerUser.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        // retrieve role
        String role = this.getRole(authentication);

        // check if it is a customer or a company user
        if (role.equals("ROLE_CUSTOMER")) {
            CustomerUserDetailsImpl userDetails = (CustomerUserDetailsImpl) authentication.getPrincipal();

            return ResponseEntity.ok(new JwtResponse(jwt,
                    userDetails.getId(),
                    role));
        }
        else {
            CompanyUserDetailsImpl userDetails = (CompanyUserDetailsImpl) authentication.getPrincipal();

            return ResponseEntity.ok(new JwtResponse(jwt,
                    userDetails.getId(),
                    role));
        }
    }

    // EmployeeUser registration method
    public ResponseEntity registerEmployeeUser(CompanyUserSignUpRequestDTO signUpCompanyUser, RoleName roleName) {
        // first check whether the user already exists
        String errorMessage = isNewUserUnique(signUpCompanyUser);
        if (!errorMessage.equals("")) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse(errorMessage));
        }

        boolean isActive = true;
        Instant now = Instant.now();

        // Create new user's account
        CompanyUser user = new CompanyUser(
                signUpCompanyUser.getName(),
                signUpCompanyUser.getSurname(),
                signUpCompanyUser.getUsername(),
                encoder.encode(signUpCompanyUser.getPassword()),
                signUpCompanyUser.getEmail(),
                now,
                isActive);

        Role foundRole = roleRepository.findByRoleName(roleName.name())
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        user.setRole(foundRole);

        companyUserRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    // CustomerUser registration method
    public ResponseEntity registerCustomerUser(CustomerUserSignUpRequestDTO signUpCustomerUser) {
        // first check whether the user already exists
        String errorMessage = isNewUserUnique(signUpCustomerUser);
        if (!errorMessage.equals("")) {
            return ResponseEntity
                .badRequest()
                .body(new MessageResponse(errorMessage));
        }

        boolean isActive = true;
        Instant now = Instant.now();

        // Create new user's account
        CustomerUser user = new CustomerUser(
                signUpCustomerUser.getName(),
                signUpCustomerUser.getSurname(),
                signUpCustomerUser.getUsername(),
                encoder.encode(signUpCustomerUser.getPassword()),
                signUpCustomerUser.getEmail(),
                now,
                isActive);

        Role role;

        Role foundRole = roleRepository.findByRoleName(RoleName.ROLE_CUSTOMER.name())
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        role = foundRole;

        user.setRole(role);

        customerUserRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    public ResponseEntity changeCompanyUserRole(Long companyUserId, RoleName role) {
        Optional<CompanyUser> foundUser = this.companyUserRepository.findById(companyUserId);

        if(foundUser.isPresent()) {
            Optional<Role> newRole = this.roleRepository.findByRoleName(role.name());

            if(newRole.isPresent()) {
                CompanyUser user = foundUser.get();
                user.setRole(newRole.get());

                this.companyUserRepository.save(user);

                return new ResponseEntity(HttpStatus.OK);
            }
            else
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        else
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

    }

    /* helper functions */

    private String getRole(Authentication authentication) {
        List<String> list = new ArrayList<>();
        for (GrantedAuthority item : authentication.getAuthorities()) {
            String authority = item.getAuthority();
            list.add(authority);
        }
        return list.get(0);
    }

    private String isNewUserUnique(CustomerUserSignUpRequestDTO signUpCustomerUser) {
        String errorMessage = "";

        if ( Boolean.TRUE.equals(customerUserRepository.existsByUsername(signUpCustomerUser.getUsername())) ) {
            return "Error: Username is already taken!";
        }

        if ( Boolean.TRUE.equals(customerUserRepository.existsByEmail(signUpCustomerUser.getEmail())) ) {
            return "Error: Email is already in use!";
        }

        return errorMessage;
    }

    private String isNewUserUnique(CompanyUserSignUpRequestDTO signUpCustomerUser) {
        String errorMessage = "";

        if ( Boolean.TRUE.equals(companyUserRepository.existsByUsername(signUpCustomerUser.getUsername())) ) {
            return "Error: Username is already taken!";
        }

        if ( Boolean.TRUE.equals(companyUserRepository.existsByEmail(signUpCustomerUser.getEmail())) ) {
            return "Error: Email is already in use!";
        }

        return errorMessage;
    }
}
