package fontys.fhict.nl.upcatalog.service;

import fontys.fhict.nl.upcatalog.dto.CartDTO;
import fontys.fhict.nl.upcatalog.model.cart.Cart;
import fontys.fhict.nl.upcatalog.model.cart.ProductCart;
import fontys.fhict.nl.upcatalog.model.item.Product;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import fontys.fhict.nl.upcatalog.repository.CartRepository;
import fontys.fhict.nl.upcatalog.repository.ProductCartRepository;
import fontys.fhict.nl.upcatalog.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CartService {
    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductCartRepository productCartRepository;

    @Autowired
    private ProductRepository productRepository;

    public Cart getEnabledCart(Long customerId) {
        // get last enabled cart id
        Long cartId = this.cartRepository.getLastEnabledCartId(customerId);

        Optional<Cart> foundCart = this.cartRepository.findById(cartId);

        return foundCart.orElse(null);
    }

    public List<CartDTO> getEnabledCartList(Long customerId) {
        List<CartDTO> list = new ArrayList<>();

        // get last enabled cart id
        Long cartId = this.cartRepository.getLastEnabledCartId(customerId);

        // get enabled cart's product list
        if(cartId != null) {
            List<Product> productList = this.productRepository.getAllProductsFromEnabledCart(cartId);
                List<ProductCart> quantities = this.productCartRepository.getQuantities(cartId);

            Map<Long, Product> products = new HashMap<>();

            for(Product item: productList) {
                products.put(item.getId(), item);
            }

            for(ProductCart item : quantities) {
                list.add(new CartDTO(
                        new CustomerUser(customerId),
                        item.getQuantity(),
                        products.get(item.getProduct().getId())
                ));
            }
        }
        return list;
    }

    public ResponseEntity addCartElement(CartDTO cartDTO) {
        Long customerId = cartDTO.getCustomerUser().getId();

        // todo what if it returns null
        Long cartId = this.cartRepository.getLastEnabledCartId(customerId);

        if(cartId == null) {
            // in case there is no active new cart for the user

            // create new cart
            Cart newCart = new Cart(cartDTO.getCustomerUser());
            this.cartRepository.save(newCart);

            // save the first product in the cart
            this.productCartRepository.save(new ProductCart(cartDTO.getProduct(), newCart, cartDTO.getQuantity()));
        }
        else {
            // in case there is one active cart for the user
            Optional<Cart> foundCart = this.cartRepository.findById(cartId);

            if(foundCart.isPresent()) {
                this.productCartRepository.save(new ProductCart(cartDTO.getProduct(), foundCart.get(), cartDTO.getQuantity()));
            }
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity changeCartElementQuantity(CartDTO cartDTO) {
        Long customerId = cartDTO.getCustomerUser().getId();
        Long cartId = this.cartRepository.getLastEnabledCartId(customerId);

        Optional<Cart> foundCart = this.cartRepository.findById(cartId);

        if(foundCart.isPresent()) {
            Optional<ProductCart> foundProductCart = this.productCartRepository.findByCartId(cartId, cartDTO.getProduct().getId());

            if(foundProductCart.isPresent()) {
                ProductCart newProductCart = foundProductCart.get();
                newProductCart.setQuantity(cartDTO.getQuantity());
                this.productCartRepository.save(newProductCart);
            }
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public boolean disableCurrentlyUsingCart(Long customerId) {
        // get last enabled cart id
        Long cartId = this.cartRepository.getLastEnabledCartId(customerId);

        return Boolean.TRUE.equals(this.cartRepository.disableLastEnabledCart(cartId));
    }
}


