package fontys.fhict.nl.upcatalog.service;

import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import fontys.fhict.nl.upcatalog.model.user.Role;
import fontys.fhict.nl.upcatalog.payload.response.JwtResponse;
import fontys.fhict.nl.upcatalog.payload.response.MessageResponse;
import fontys.fhict.nl.upcatalog.repository.CustomerUserRepository;
import fontys.fhict.nl.upcatalog.repository.RoleRepository;
import fontys.fhict.nl.upcatalog.security.JWT.JwtUtils;
import fontys.fhict.nl.upcatalog.service.userDetails.CustomerUserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.function.EntityResponse;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerUserService {
    @Autowired
    private CustomerUserRepository customerUserRepository;

    public List<CustomerUser> getAllCustomerUsers() {
        return this.customerUserRepository.findAll();
    }

    public Optional<CustomerUser> findCustomerUserById(Long id) {
        return this.customerUserRepository.findById(id);
    }

    // todo update method
    public CustomerUser updateCustomerUserById(CustomerUser user) {
        return this.customerUserRepository.save(user);
    }

    // TOdo disable method
}
