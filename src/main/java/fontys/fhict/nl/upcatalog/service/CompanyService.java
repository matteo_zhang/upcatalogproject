package fontys.fhict.nl.upcatalog.service;

import fontys.fhict.nl.upcatalog.dto.CompanyDTO;
import fontys.fhict.nl.upcatalog.model.Company;
import fontys.fhict.nl.upcatalog.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    public List<Company> getAllCompany() {
        return this.companyRepository.findAll();
    }

    public Company addCompany(CompanyDTO company) {
        Company newCompany = new Company();

        newCompany.setName(company.getName());
        newCompany.setEmail(company.getEmail());
        newCompany.setAddress(company.getAddress());
        newCompany.setTelephoneNumber(company.getTelephoneNumber());
        newCompany.setVATNumber(company.getVATNumber());

        return this.companyRepository.save(newCompany);
    }
}
