package fontys.fhict.nl.upcatalog.service.userDetails;

import fontys.fhict.nl.upcatalog.model.user.CompanyUser;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import fontys.fhict.nl.upcatalog.repository.CompanyUserRepository;
import fontys.fhict.nl.upcatalog.repository.CustomerUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UpCatalogUserDetailsService implements UserDetailsService {
    @Autowired
    CompanyUserRepository companyUserRepository;

    @Autowired
    CustomerUserRepository customerUserRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        /* The UpCatalog Database has 2 tables, one for company users and one for customer users.
        *  The authentication process use this method to find users.
        */

        // find company user
        Optional<CompanyUser> optionalCompanyUser = companyUserRepository.findByUsername(username);
        if (optionalCompanyUser.isPresent()) {
            // return company user with authorities
            return CompanyUserDetailsImpl.build(optionalCompanyUser.get());
        }

        // find customer user
        Optional<CustomerUser> optionalCustomerUser = customerUserRepository.findByUsername(username);
        if (optionalCustomerUser.isPresent()) {
            // return customer user with authorities
            return CustomerUserDetailsImpl.build(optionalCustomerUser.get());
        }

        throw new UsernameNotFoundException("User Not Found with username: " + username);
    }
}
