package fontys.fhict.nl.upcatalog.payload.response;

import lombok.Data;

@Data
public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private Long id;
    private String role;

    public JwtResponse(String accessToken, Long id, String role) {
        this.token = accessToken;
        this.id = id;
        this.role = role;
    }


}
