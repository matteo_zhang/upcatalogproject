package fontys.fhict.nl.upcatalog.model.user;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.Instant;

/*
 * Employee User is considered a company user type like manager and admin. They difference from
 * Customer user by where they authenticate
 * */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@Table(name = "company_user")
public class CompanyUser extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public CompanyUser(String name, String surname, String username, String password, String email, Instant createdDate, boolean enabled) {
        super(name, surname, username, password, email, createdDate, enabled);
    }
}
