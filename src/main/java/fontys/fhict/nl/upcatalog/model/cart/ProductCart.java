package fontys.fhict.nl.upcatalog.model.cart;

import fontys.fhict.nl.upcatalog.model.item.Product;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;

/* this class is the join table between Product and Cart tables.
*
*/

@Entity
@Data
@NoArgsConstructor
public class ProductCart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Product product;

    @ManyToOne
    private Cart cart;

    // Reminder: every product can be purchased by stock quantities
    @Min(1)
    private int quantity;

    public ProductCart(Product product, Cart cart, @Min(1) int quantity) {
        this.product = product;
        this.cart = cart;
        this.quantity = quantity;
    }
}
