package fontys.fhict.nl.upcatalog.model.item.profile;

import fontys.fhict.nl.upcatalog.model.item.profile.Field;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class FieldTextBox extends Field {
    private String text;


}
