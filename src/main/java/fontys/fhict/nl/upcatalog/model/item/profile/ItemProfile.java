package fontys.fhict.nl.upcatalog.model.item.profile;

import java.util.List;

public class ItemProfile {
    private String id;
    private String name;
    protected List<Field> fields;
}
