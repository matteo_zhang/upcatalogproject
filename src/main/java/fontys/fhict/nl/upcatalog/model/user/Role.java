package fontys.fhict.nl.upcatalog.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String roleName;

    @OneToMany(mappedBy = "role")
    @JsonIgnore
    private List<CustomerUser> customerUsers;

    @OneToMany(mappedBy = "role")
    @JsonIgnore
    private List<CompanyUser> companyUsers;

    public Role(Long id, String roleName) {
        this.id = id;
        this.roleName = roleName;
    }
}

