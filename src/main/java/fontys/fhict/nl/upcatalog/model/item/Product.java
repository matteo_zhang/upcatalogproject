package fontys.fhict.nl.upcatalog.model.item;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fontys.fhict.nl.upcatalog.interfaces.Shippable;
import fontys.fhict.nl.upcatalog.model.cart.ProductCart;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false) // decorator used with inheritance
@Entity
public class Product extends Item implements Shippable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    protected String serialNumber;

    private double sellPrice;

    private int quantityPerStock;

    @OneToMany(mappedBy = "product")
    @JsonIgnore
    private Set<ProductCart> productCart = new HashSet<ProductCart>();

    @Override
    public double calculatePricePerStock() {
        return this.sellPrice * this.quantityPerStock;
    }
}
