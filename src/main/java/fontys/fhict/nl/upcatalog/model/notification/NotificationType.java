package fontys.fhict.nl.upcatalog.model.notification;

public enum NotificationType {
    NewCustomerAccountRequest, NewCompanyAccountRequest, RunningOutOfStock
}
