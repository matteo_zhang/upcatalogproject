package fontys.fhict.nl.upcatalog.model.user;

public enum RoleName {
    ROLE_CUSTOMER, ROLE_EMPLOYEE, ROLE_MANAGER, ROLE_ADMIN
}
