package fontys.fhict.nl.upcatalog.model.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class User {
    @NotBlank()
    @Size(max = 50)
    protected String name;

    @NotBlank()
    @Size(max = 50)
    protected String surname;

    @NotBlank()
    @Size(max = 20)
    protected String username;

    @NotBlank()
    @Size(max = 120)
    protected String password;

    @NotBlank()
    @Size(max = 50)
    protected String email;

    @Size(max = 50)
    protected Instant createdDate;

    @LastModifiedDate
    protected Instant modifiedDate;

    protected boolean enabled;

    @ManyToOne()
    protected Role role;

    protected User(@NotBlank() @Size(max = 50) String name, @NotBlank() @Size(max = 50) String surname, @NotBlank() @Size(max = 20) String username, @NotBlank() @Size(max = 120) String password, @NotBlank() @Size(max = 50) String email, @NotBlank() @Size(max = 50) Instant createdDate, boolean enabled) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.email = email;
        this.createdDate = createdDate;
        this.modifiedDate = createdDate;
        this.enabled = enabled;
    }
}
