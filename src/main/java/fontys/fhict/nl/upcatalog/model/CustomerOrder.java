package fontys.fhict.nl.upcatalog.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fontys.fhict.nl.upcatalog.interfaces.InvoiceInterface;
import fontys.fhict.nl.upcatalog.model.cart.Cart;
import fontys.fhict.nl.upcatalog.model.cart.ProductCart;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerOrder implements InvoiceInterface {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Cart cart;

    @ManyToOne
    private CustomerUser customer;

    private double totalAmount;

    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public CustomerOrder(Cart cart, CustomerUser customer, double totalAmount) {
        this.cart = cart;
        this.customer = customer;
        this.totalAmount = totalAmount;
    }

    @Override
    public double getOrderTotalAmount() {
        return this.totalAmount;
    }

    @Override
    public Cart getCart() {
        return this.cart;
    }

    @Override
    public CustomerUser getCustomer() {
        return this.customer;
    }
}