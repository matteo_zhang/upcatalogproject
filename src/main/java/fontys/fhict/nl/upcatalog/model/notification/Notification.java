package fontys.fhict.nl.upcatalog.model.notification;

import com.sun.istack.Nullable;
import fontys.fhict.nl.upcatalog.interfaces.UpCatalogNotification;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@Data
@NoArgsConstructor
@MappedSuperclass
public abstract class Notification implements UpCatalogNotification {
    @Nullable
    private String message;
    private NotificationType notificationType;
    private boolean result;

    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public Notification(String message, NotificationType notificationType) {
        this.message = message;
        this.notificationType = notificationType;
        this.result = false;
    }

    @Override
    public boolean send(String message) {
        return false;
    }
}
