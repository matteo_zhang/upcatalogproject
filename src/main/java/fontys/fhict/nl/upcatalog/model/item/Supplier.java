package fontys.fhict.nl.upcatalog.model.item;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fontys.fhict.nl.upcatalog.model.BusinessOrganization;
import fontys.fhict.nl.upcatalog.model.item.RawMaterial;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
public class Supplier extends BusinessOrganization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "supplier")
    @JsonIgnore
    private List<RawMaterial> rawMaterialList;

}