package fontys.fhict.nl.upcatalog.model.item;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
public abstract class Item {
//    @NotBlank
    protected String name;

    @Column(columnDefinition="TEXT") /*@NotBlank*/
    protected String description;

    @ManyToOne()
    protected Category category;

    @Min(1) /*@NotBlank*/
    protected double boughtPrice;
    @Min(1)
    protected int quantity;
    @Min(1)
    protected int minimumThreshold;

    protected String imagePath;
//    protected ItemProfile itemProfile;

    protected boolean isRemoved;

    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;


    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }
}
