package fontys.fhict.nl.upcatalog.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fontys.fhict.nl.upcatalog.model.Company;
import fontys.fhict.nl.upcatalog.model.CustomerOrder;
import fontys.fhict.nl.upcatalog.model.cart.Cart;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

/*
 * Customer User is NOT considered a COMPANY user type like employee or manager. The main difference is that
 * they authenticate at the main login page and have restricted authorizations.
 * */

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customer_user",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")})

public class CustomerUser extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne()
    private Company company;

    @OneToMany
    private List<Cart> carts;

    @OneToMany(mappedBy = "customer")
    @JsonIgnore
    private List<CustomerOrder> customerOrders;

    public CustomerUser(Long id) {
        this.id = id;
    }

    @Builder
    public CustomerUser(String name, String surname, String username, String password, String email, Instant createdDate, boolean enabled, Role role) {
        super(name, surname, username, password, email, createdDate, createdDate, enabled, role);
    }

    // create new user in database
    public CustomerUser(String name, String surname, String username, String password, String email, Instant createdDate, boolean enabled) {
        super(name, surname, username, password, email, createdDate, enabled);
    }

}
