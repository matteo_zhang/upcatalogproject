package fontys.fhict.nl.upcatalog.model.item.profile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class Field {
    protected Long id;
    protected String title;
}
