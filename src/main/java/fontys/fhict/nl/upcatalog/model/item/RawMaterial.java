package fontys.fhict.nl.upcatalog.model.item;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper=false) // decorator used with inheritance
@Entity
public class RawMaterial extends Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne()
    private Supplier supplier;
}
