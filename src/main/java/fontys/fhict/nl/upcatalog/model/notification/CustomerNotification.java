package fontys.fhict.nl.upcatalog.model.notification;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/* customer notifications are used when new customers want to use the website; they type their info,
* then data is carried by the notification entity in database waiting being accepted or rejected
* by company managers.
*
* The notification contains customer's business company infos plus his own personal info.
*/

@Entity
@AllArgsConstructor
@Data
@NoArgsConstructor
public class CustomerNotification extends Notification {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // customer info
    @NotBlank()
    private String customerName;
    @NotBlank()
    private String customerSurname;
    @NotBlank()
    private String customerEmail;
    @NotBlank()
    private String username;
    @NotBlank()
    private String password;

    // company data
    @NotBlank()
    private String companyName;
    @NotBlank()
    private String VATNumber;
    @NotBlank()
    private String address;
    @NotBlank()
    private Integer telephoneNumber;
    @NotBlank()
    private String companyEmail;

    public CustomerNotification(String message, NotificationType notificationType, @NotBlank() String customerName, @NotBlank() String customerSurname, @NotBlank() String username, @NotBlank() String password, @NotBlank() String customerEmail, @NotBlank() String companyName, @NotBlank() String VATNumber, @NotBlank() String address, @NotBlank() Integer telephoneNumber, @NotBlank() String companyEmail) {
        super(message, notificationType);
        this.customerName = customerName;
        this.customerSurname = customerSurname;
        this.username = username;
        this.password = password;
        this.customerEmail = customerEmail;
        this.companyName = companyName;
        this.VATNumber = VATNumber;
        this.address = address;
        this.telephoneNumber = telephoneNumber;
        this.companyEmail = companyEmail;
    }

    @Override
    public boolean send(String message) {
        return super.send(message);
    }
}

