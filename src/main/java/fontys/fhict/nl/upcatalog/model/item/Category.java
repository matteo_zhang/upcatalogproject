package fontys.fhict.nl.upcatalog.model.item;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fontys.fhict.nl.upcatalog.model.item.Product;
import fontys.fhict.nl.upcatalog.model.item.RawMaterial;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    private List<Product> products;

    @OneToMany(mappedBy = "category")
    @JsonIgnore
    private List<RawMaterial> rawMaterials;
}
