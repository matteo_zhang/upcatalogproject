package fontys.fhict.nl.upcatalog.dto;

import fontys.fhict.nl.upcatalog.model.cart.Cart;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

// this dto is used to send all items belonging to an order
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {
    private Cart cart;
    private CustomerUser customer;
    private Set<ProductCartDTO> productCart;
    private double totalAmount;
}
