package fontys.fhict.nl.upcatalog.dto;

import fontys.fhict.nl.upcatalog.model.BusinessOrganization;
import fontys.fhict.nl.upcatalog.model.notification.Notification;
import fontys.fhict.nl.upcatalog.model.notification.NotificationType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerNotificationDTO {
    // customer info
    private String customerName;
    private String customerSurname;
    private String username;
    private String password;
    private String customerEmail;

    // company data
    private String companyName;
    private String VATNumber;
    private String address;
    private Integer telephoneNumber;
    private String companyEmail;
}