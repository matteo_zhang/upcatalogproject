package fontys.fhict.nl.upcatalog.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSignInRequestDTO {
    @NotBlank
    private String username;

    @NotBlank
    private String password;
}
