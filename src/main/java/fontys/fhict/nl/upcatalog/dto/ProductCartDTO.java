package fontys.fhict.nl.upcatalog.dto;

import fontys.fhict.nl.upcatalog.model.item.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCartDTO {
    private int quantity;
    private Product product;
}
