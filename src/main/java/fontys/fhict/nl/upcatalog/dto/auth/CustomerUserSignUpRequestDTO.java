package fontys.fhict.nl.upcatalog.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

// this DTO class is used for registration of company users

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerUserSignUpRequestDTO {
    @NotBlank()
    private String name;

    @NotBlank()
    private String surname;

    @NotBlank()
    private String username;

    @NotBlank()
    private String password;

    @NotBlank()
    private String email;
}
