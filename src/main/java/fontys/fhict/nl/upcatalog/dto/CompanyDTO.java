package fontys.fhict.nl.upcatalog.dto;

import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDTO {
    private String name;
    private String VATNumber;
    private String address;
    private Integer telephoneNumber;
    private String email;
}
