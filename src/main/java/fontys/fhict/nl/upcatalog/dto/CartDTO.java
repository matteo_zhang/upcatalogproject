package fontys.fhict.nl.upcatalog.dto;

import fontys.fhict.nl.upcatalog.model.item.Product;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartDTO {
    CustomerUser customerUser;
    int quantity;
    Product product;
}
