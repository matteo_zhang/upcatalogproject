package fontys.fhict.nl.upcatalog.interfaces;

public interface UpCatalogNotification {
    // TODO: 11/12/2020 create different interfaces that allows to send broadcastly or unicastly 
    /* has to send the message to the receiver*/
    boolean send(String message);
}
