package fontys.fhict.nl.upcatalog.interfaces;

import fontys.fhict.nl.upcatalog.model.cart.Cart;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;

public interface InvoiceInterface {
    public CustomerUser getCustomer();
    public double getOrderTotalAmount();
    public Cart getCart();
}
