package fontys.fhict.nl.upcatalog.interfaces;

public interface Shippable {
    public double calculatePricePerStock();
}
