package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.notification.CustomerNotification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerNotificationRepository extends JpaRepository<CustomerNotification, Long> {
}
