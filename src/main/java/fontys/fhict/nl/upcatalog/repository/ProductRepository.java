package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.item.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query(value = "SELECT p.* FROM product_cart pc INNER JOIN product p ON pc.product_id = p.id WHERE pc.cart_id = :cart_id"
            , nativeQuery = true)
    List<Product> getAllProductsFromEnabledCart(@Param("cart_id") Long cartId);
}
