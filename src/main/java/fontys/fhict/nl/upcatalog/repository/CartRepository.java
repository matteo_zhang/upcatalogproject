package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.cart.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CartRepository extends JpaRepository<Cart, Long> {
    @Query(value = "SELECT MAX(id) FROM cart WHERE customer_user_id = :customer_id AND enabled = 1"
            , nativeQuery = true)
    Long getLastEnabledCartId(@Param("customer_id") Long customerId);

    @Query(value = "UPDATE cart SET enabled=0 WHERE id = :cart_id"
            ,nativeQuery = true)
    Boolean disableLastEnabledCart(@Param("cart_id") Long cartId);
}
