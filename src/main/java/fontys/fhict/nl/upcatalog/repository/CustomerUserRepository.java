package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CustomerUserRepository extends JpaRepository<CustomerUser, Long> {
    // added from tutorial
//    CustomerUser findByUsername(String username);

    @Query(value = "SELECT u.*, r.role_name FROM customer_user u INNER JOIN role r ON u.role_id = r.id WHERE u.username = :username AND r.role_name = 'ROLE_CUSTOMER'",
            nativeQuery = true)
    Optional<CustomerUser> findByUsername(@Param("username") String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

}
