package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.item.RawMaterial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RawMaterialRepository extends JpaRepository<RawMaterial, Long> {
}
