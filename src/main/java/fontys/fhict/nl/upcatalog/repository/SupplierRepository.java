package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.item.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {
}
