package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.CustomerOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Long> {
    @Query(value = "SELECT * FROM customer_order WHERE customer_id = :id"
            , nativeQuery = true)
    List<CustomerOrder> findAllByCustomerUserId(@Param("id") Long customerUserId);
}
