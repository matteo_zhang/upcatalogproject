package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
