package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.user.CompanyUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

// company user repo
public interface CompanyUserRepository extends JpaRepository<CompanyUser, Long> {
    // code different query method for employee, manager and admin users
    @Query(value = "SELECT u.*, r.role_name FROM company_user u INNER JOIN role r ON u.role_id = r.id WHERE u.username = :username AND r.role_name <> 'ROLE_CUSTOMER'",
            nativeQuery = true)
    Optional<CompanyUser> findByUsername(@Param("username") String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

//    @Query("SELECT u FROM customer_user u join u.roles r WHERE u.username = :username and r.roleName='USER'")
//        List<User> findManagerUserByUsername(@Param("username") String username);
//
//     @Query("SELECT u FROM Users u join u.roles r WHERE u.username = :username and r.roleName='USER'")
//        public List<Users> findUser(@Param("username") String username);
}
