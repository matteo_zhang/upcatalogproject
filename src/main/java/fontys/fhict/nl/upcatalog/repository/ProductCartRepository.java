package fontys.fhict.nl.upcatalog.repository;

import fontys.fhict.nl.upcatalog.model.cart.ProductCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ProductCartRepository extends JpaRepository<ProductCart, Long> {
    @Query(value = "SELECT * FROM product_cart WHERE cart_id = :cart_id AND product_id = :prod_id"
            , nativeQuery = true)
    Optional<ProductCart> findByCartId(@Param("cart_id") Long cartId, @Param("prod_id") Long ProductId);

    @Query(value = "SELECT * FROM product_cart WHERE cart_id = :cart_id"
            , nativeQuery = true)
    List<ProductCart> getQuantities(@Param("cart_id") Long cartId);
}
