package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.dto.CustomerNotificationDTO;
import fontys.fhict.nl.upcatalog.model.notification.CustomerNotification;
import fontys.fhict.nl.upcatalog.service.CustomerNotificationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/customer/notification")
public class CustomerNotificationController {

    @Autowired
    CustomerNotificationService customerNotificationService;

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public List<CustomerNotification> getAllCustomerNotification() {
        return this.customerNotificationService.getAllCustomerNotification();
    }

    @PostMapping
    @PreAuthorize("hasRole('CUSTOMER')")
    public CustomerNotification addCustomerNotification(@RequestBody @Valid @NotNull CustomerNotificationDTO customerNotificationDTO) {
        return this.customerNotificationService.addCustomerNotification(customerNotificationDTO);
    }

}
