package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.dto.CompanyDTO;
import fontys.fhict.nl.upcatalog.model.Company;
import fontys.fhict.nl.upcatalog.service.CompanyService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/customer/company")
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @GetMapping
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public List<Company> getAllCompany() {
        return this.companyService.getAllCompany();
    }

    @PostMapping
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public Company addCompany(@Valid @NotNull @RequestBody CompanyDTO company) {
        return this.companyService.addCompany(company);
    }
}
