package fontys.fhict.nl.upcatalog.controller;

import com.sun.istack.NotNull;
import fontys.fhict.nl.upcatalog.model.item.RawMaterial;
import fontys.fhict.nl.upcatalog.service.RawMaterialService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/raw-material")
@AllArgsConstructor
public class RawMaterialController {
    @Autowired
    private RawMaterialService rawMaterialService;

    @GetMapping()
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public List<RawMaterial> getAllRawMaterials() {
        return  rawMaterialService.getAllRawMaterials();
    }

    @PostMapping()
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public RawMaterial addRawMaterial(@Valid @NotNull @RequestBody RawMaterial rawMaterial) {
        return rawMaterialService.addRawMaterial(rawMaterial);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public Optional<RawMaterial> findRawMaterialById(@PathVariable("id") Long id) {
        return rawMaterialService.findRawMaterialById(id);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public ResponseEntity<?> updateRawMaterial(@RequestBody RawMaterial rawMaterial, @PathVariable("id") Long id) {
        return rawMaterialService.updateRawMaterial(rawMaterial, id);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public ResponseEntity.BodyBuilder removeRawMaterial(@PathVariable("id") Long id) {
        return rawMaterialService.removeRawMaterial(id);
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity.BodyBuilder deleteRawMaterial(@PathVariable("id") Long id) {
        return rawMaterialService.deleteRawMaterial(id);
    }
}
