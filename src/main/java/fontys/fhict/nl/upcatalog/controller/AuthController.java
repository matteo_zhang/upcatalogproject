package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.dto.auth.CompanyUserSignUpRequestDTO;
import fontys.fhict.nl.upcatalog.dto.auth.CustomerUserSignUpRequestDTO;
import fontys.fhict.nl.upcatalog.dto.auth.UserSignInRequestDTO;
import fontys.fhict.nl.upcatalog.model.user.RoleName;
import fontys.fhict.nl.upcatalog.service.UpCatalogUserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private UpCatalogUserService upCatalogUserService;

    @PostMapping("/signin")
    public ResponseEntity signIn(@Valid @RequestBody UserSignInRequestDTO loginRequest) {
        return this.upCatalogUserService.authenticateUser(loginRequest);
    }

    @PostMapping("/signup")
    @PreAuthorize("hasRole('MANAGER') || hasRole('ADMIN')")
    public ResponseEntity registerCustomerUser(@Valid @RequestBody CustomerUserSignUpRequestDTO signUpRequest) {
        return this.upCatalogUserService.registerCustomerUser(signUpRequest);
    }

    @PostMapping("/employee/signup")
    @PreAuthorize("hasRole('MANAGER') || hasRole('ADMIN')")
    public ResponseEntity registerEmployeeUser(@Valid @RequestBody CompanyUserSignUpRequestDTO signUpRequest) {
        return this.upCatalogUserService.registerEmployeeUser(signUpRequest, RoleName.ROLE_EMPLOYEE);
    }

    @PostMapping("/manager/signup")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity registerManagerUser(@Valid @RequestBody CompanyUserSignUpRequestDTO signUpRequest) {
        return this.upCatalogUserService.registerEmployeeUser(signUpRequest, RoleName.ROLE_MANAGER);
    }
}