package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.model.user.CompanyUser;
import fontys.fhict.nl.upcatalog.service.CompanyUserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.PathParam;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/company")
@AllArgsConstructor
public class CompanyUserController {
    @Autowired
    CompanyUserService companyUserService;

    @GetMapping()
    @PreAuthorize("hasRole('MANAGER') || hasRole('ADMIN')")
    public List<CompanyUser> getAllCompanyUsers() {
        return this.companyUserService.getAllCompanyUsers();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('MANAGER') || hasRole('ADMIN')")
    public Optional<CompanyUser> findCompanyUsersById(@PathVariable("id") Long id) {
        return this.companyUserService.findCompanyUserById(id);
    }

    @PutMapping()
    @PreAuthorize("hasRole('MANAGER') || hasRole('ADMIN')")
    public CompanyUser updateCompanyUsersById(@RequestBody CompanyUser user) {
        return this.companyUserService.updateCompanyUsersById(user);
    }
}
