package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.dto.OrderDTO;
import fontys.fhict.nl.upcatalog.model.CustomerOrder;
import fontys.fhict.nl.upcatalog.service.CustomerOrderService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/customer/order")
@AllArgsConstructor
public class CustomerOrderController {
    @Autowired
    CustomerOrderService customerOrderService;

    @GetMapping()
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public List<OrderDTO> getOrderList() {
        return this.customerOrderService.getOrderList();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public List<OrderDTO> getOrderListByCustomerId(@PathVariable("id") Long customerId) {
        return this.customerOrderService.getOrderListByCustomerId(customerId);
    }

    @PutMapping()
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public ResponseEntity generateOrder(@RequestBody Long customerId) {
        return this.customerOrderService.generateOrder(customerId);
    }
}
