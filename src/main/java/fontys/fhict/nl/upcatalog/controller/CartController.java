package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.dto.CartDTO;
import fontys.fhict.nl.upcatalog.model.cart.Cart;
import fontys.fhict.nl.upcatalog.model.item.Product;
import fontys.fhict.nl.upcatalog.service.CartService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/cart")
@AllArgsConstructor
public class CartController {
    @Autowired
    private CartService cartService;

    @GetMapping("/{id}")
    public List<CartDTO> getAllEnabledCartList(@PathVariable("id") Long id) {
        return this.cartService.getEnabledCartList(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity addCartElement(@RequestBody CartDTO cartDTO) {
        return this.cartService.addCartElement(cartDTO);
    }

    @PutMapping
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity changeCartElement(@RequestBody CartDTO cartDTO) {
        return this.cartService.changeCartElementQuantity(cartDTO);
    }
}
