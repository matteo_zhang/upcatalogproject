package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.model.item.Category;
import fontys.fhict.nl.upcatalog.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("api/category")
public class CategoryController {
    @Autowired
    CategoryService service;

    @GetMapping()
    public List<Category> getAllCategories() {
        return service.getAllCategories();
    }
}
