package fontys.fhict.nl.upcatalog.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import fontys.fhict.nl.upcatalog.service.CustomerUserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/customer")
@AllArgsConstructor
public class CustomerUserController {
    @Autowired
    private CustomerUserService customerUserService;

    @GetMapping()
    @PreAuthorize("hasRole('MANAGER') or hasRole('ADMIN')")
    public List<CustomerUser> getAllCustomerUsers() {
        return this.customerUserService.getAllCustomerUsers();
    }

    @GetMapping("/{id}")
    public Optional<CustomerUser> findCustomerUserById(@PathVariable("id") Long id) {
        return this.customerUserService.findCustomerUserById(id);
    }

    @PutMapping()
    public CustomerUser updateCustomerUserById(@RequestBody CustomerUser user) {
        return this.customerUserService.updateCustomerUserById(user);
    }
}
