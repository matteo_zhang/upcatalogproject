package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.model.user.Role;
import fontys.fhict.nl.upcatalog.repository.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/role")
@AllArgsConstructor
public class RoleController {
    @Autowired
    RoleRepository roleRepository;

    @GetMapping()
    public List<Role> getRolelist() {
        return this.roleRepository.findAll();
    }
}
