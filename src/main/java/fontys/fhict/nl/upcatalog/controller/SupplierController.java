package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.model.item.Supplier;
import fontys.fhict.nl.upcatalog.repository.SupplierRepository;
import fontys.fhict.nl.upcatalog.service.SupplierService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/supplier")
@AllArgsConstructor
public class SupplierController {
    @Autowired
    SupplierService supplierService;

    @GetMapping()
    @PreAuthorize("hasRole('EMPLOYEE') || hasRole('MANAGER') || hasRole('ADMIN')")
    public List<Supplier> getAllSupplier() {
        return this.supplierService.getAllSupplierList();
    }
}