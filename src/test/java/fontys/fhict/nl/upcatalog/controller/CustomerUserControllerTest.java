package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import fontys.fhict.nl.upcatalog.model.user.Role;
import fontys.fhict.nl.upcatalog.repository.CustomerUserRepository;
import fontys.fhict.nl.upcatalog.repository.RoleRepository;
import fontys.fhict.nl.upcatalog.security.JWT.JwtUtils;
import fontys.fhict.nl.upcatalog.service.CustomerUserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.net.URI;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class CustomerUserControllerTest {
    private MockMvc mockMvc;
    private CustomerUserController customerUserController;

    @InjectMocks
    CustomerUserService customerUserService;
    @MockBean
    CustomerUserRepository customerUserRepository;

    private final String apiUrl = "/api/customer";

    @BeforeEach
    public void setup() {
        customerUserController = new CustomerUserController(customerUserService);
        mockMvc = MockMvcBuilders.standaloneSetup(customerUserController).build();
    }

    @Test
    void getAllCustomersTest() throws Exception {
        CustomerUser loginUser = new CustomerUser();
        String username = "johndoe";
        loginUser.setUsername(username);
        loginUser.setPassword("iamjohn");

        Mockito.when(customerUserRepository.findAll()).thenReturn(Stream.of(
                loginUser, new CustomerUser()
        ).collect(Collectors.toList()));

        mockMvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].username", Matchers.is(username)));
    }

    @Test
    void findCustomerUserByIdTest() throws Exception {
        CustomerUser user = new CustomerUser();
        Long id = 10L;
        String username = "johndoe";
        user.setId(id);
        user.setUsername(username);

        Mockito.when(customerUserRepository.findById(id)).thenReturn(Optional.of(user));

        mockMvc.perform(get(apiUrl + "/10"))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.username", Matchers.is(username)));
    }
}
