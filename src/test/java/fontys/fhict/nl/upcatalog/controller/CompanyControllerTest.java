package fontys.fhict.nl.upcatalog.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fontys.fhict.nl.upcatalog.model.Company;
import fontys.fhict.nl.upcatalog.model.item.RawMaterial;
import fontys.fhict.nl.upcatalog.repository.CompanyRepository;
import fontys.fhict.nl.upcatalog.service.CompanyService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class CompanyControllerTest {
    private MockMvc mockMvc;
    private CompanyController companyController;

    @InjectMocks
    CompanyService service;

    @MockBean
    private CompanyRepository companyRepository;

    private final String apiUrl = "/api/customer/company";

    @BeforeEach
    public void setup() {
        companyController = new CompanyController(service);
        mockMvc = MockMvcBuilders.standaloneSetup(companyController).build();
    }

    @Test
    void getAllCompanyTest() throws Exception {
        Mockito.when(companyRepository.findAll()).thenReturn(Stream.of(
                new Company(), new Company(), new Company()
        ).collect(Collectors.toList()));

        mockMvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", Matchers.hasSize(3)));
    }

    @Test
    void addCompanyTest() throws Exception {
        String name = "philips";
        String VAT = "123321";

        Company company = new Company();
        company.setName(name);
        company.setVATNumber(VAT);

        Mockito.when(companyRepository.save(company)).thenReturn(company);

        mockMvc.perform(post(apiUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(mapToJson(company))))
                .andExpect(status().isOk());
    }

    private String mapToJson(Company company) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return objectMapper.writeValueAsString(company);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}