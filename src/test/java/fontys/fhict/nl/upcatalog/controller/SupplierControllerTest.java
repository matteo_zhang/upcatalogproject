package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.model.item.Supplier;
import fontys.fhict.nl.upcatalog.repository.RawMaterialRepository;
import fontys.fhict.nl.upcatalog.repository.SupplierRepository;
import fontys.fhict.nl.upcatalog.service.RawMaterialService;
import fontys.fhict.nl.upcatalog.service.SupplierService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class SupplierControllerTest {
    private MockMvc mvc;
    private SupplierController controller;

    @InjectMocks
    private SupplierService service;
    @MockBean
    private SupplierRepository repository;

    private final String apiUrl = "/api/supplier";

    @BeforeEach
    void setup() {
        controller = new SupplierController(service);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }


    @Test
    void getAllSupplier() throws Exception {
        Mockito.when(repository.findAll()).thenReturn(Stream.of(
                new Supplier(), new Supplier(), new Supplier()
        ).collect(Collectors.toList()));

        mvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", Matchers.hasSize(3)));
    }
}