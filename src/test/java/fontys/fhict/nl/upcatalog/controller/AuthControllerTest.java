package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.dto.auth.CompanyUserSignUpRequestDTO;
import fontys.fhict.nl.upcatalog.dto.auth.CustomerUserSignUpRequestDTO;
import fontys.fhict.nl.upcatalog.dto.auth.UserSignInRequestDTO;
import fontys.fhict.nl.upcatalog.model.user.CompanyUser;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import fontys.fhict.nl.upcatalog.model.user.Role;
import fontys.fhict.nl.upcatalog.model.user.RoleName;
import fontys.fhict.nl.upcatalog.repository.*;
import fontys.fhict.nl.upcatalog.security.JWT.JwtUtils;
import fontys.fhict.nl.upcatalog.service.UpCatalogUserService;
import fontys.fhict.nl.upcatalog.service.userDetails.CompanyUserDetailsImpl;
import fontys.fhict.nl.upcatalog.service.userDetails.CustomerUserDetailsImpl;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Matches;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;
import java.util.stream.Collectors;

import static common.MapToJson.mapToJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.Assert.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class AuthControllerTest {
    private MockMvc mvc;
    private AuthController controller;

    @InjectMocks
    private UpCatalogUserService service;

    @MockBean
    private CustomerUserRepository customerUserRepository;
    @MockBean
    private CompanyUserRepository companyUserRepository;
    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private AuthenticationManager authenticationManager;
    @MockBean
    private JwtUtils jwtUtils;
    @MockBean
    private BCryptPasswordEncoder encoder;

    private final String apiUrl = "/api/auth";

    // common attributes
    private final Long userId = 10L;
    private final String companyUsername = "alan";
    private final String customerUsername = "john";
    private final String email = "my@gmail.com";
    private final String password = "123456ABc";


    @BeforeEach
    public void setup() {
        controller = new AuthController(service);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();

        Mockito.when(encoder.encode(password)).thenReturn("encodedPassword");
    }

    @Test
    void signIn_As_CustomerUser() throws Exception {
        UserSignInRequestDTO userSignInRequestDTO = new UserSignInRequestDTO(customerUsername, password);

        CustomerUser customerUser = new CustomerUser();
        customerUser.setId(userId);
        customerUser.setUsername(customerUsername);

        // overrided Authentication class
        Authentication mockAuthentication = new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                Set<Role> roles = new HashSet<>();
                roles.add(new Role(1L, "ROLE_CUSTOMER"));

                List<GrantedAuthority> authorities = roles.stream()
                        .map(role -> new SimpleGrantedAuthority(role.getRoleName()))
                        .collect(Collectors.toList());
                return authorities;
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                CustomerUserDetailsImpl user = new CustomerUserDetailsImpl(userId, customerUsername, password, this.getAuthorities());
                return user;
            }

            @Override
            public boolean isAuthenticated() {
                return false;
            }

            @Override
            public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

            }

            @Override
            public String getName() {
                return null;
            }
        };

        Mockito.when(companyUserRepository.findByUsername(companyUsername)).thenReturn(null);
        Mockito.when(customerUserRepository.findByUsername(customerUsername)).thenReturn(Optional.of(customerUser));
        Mockito.when(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                customerUsername, password))).thenReturn(mockAuthentication);
        Mockito.when(jwtUtils.generateJwtToken(mockAuthentication)).thenReturn("myAwesomeFakeToken");

        mvc.perform(post(apiUrl + "/signin")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Objects.requireNonNull(mapToJson(userSignInRequestDTO))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(Integer.parseInt(userId.toString()))));
    }

    @Test
    void registerCustomerUser() {
        CustomerUserSignUpRequestDTO customerUserSignUpRequestDTO = new CustomerUserSignUpRequestDTO();
        customerUserSignUpRequestDTO.setUsername(customerUsername);
        customerUserSignUpRequestDTO.setEmail(email);

        Mockito.when(customerUserRepository.existsByUsername(customerUserSignUpRequestDTO.getUsername())).thenReturn(false);
        Mockito.when(customerUserRepository.existsByEmail(customerUserSignUpRequestDTO.getEmail())).thenReturn(false);
        Mockito.when(roleRepository.findByRoleName(RoleName.ROLE_CUSTOMER.name())).thenReturn(Optional.of(new Role()));

        ResponseEntity<String> response = this.controller.registerCustomerUser(customerUserSignUpRequestDTO);
        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    void registerEmployeeUser() {
        CompanyUserSignUpRequestDTO companyUserSignUpRequestDTO = new CompanyUserSignUpRequestDTO();
        companyUserSignUpRequestDTO.setUsername(companyUsername);
        companyUserSignUpRequestDTO.setEmail(email);

        Mockito.when(customerUserRepository.existsByUsername(companyUserSignUpRequestDTO.getUsername())).thenReturn(false);
        Mockito.when(customerUserRepository.existsByEmail(companyUserSignUpRequestDTO.getEmail())).thenReturn(false);
        Mockito.when(roleRepository.findByRoleName(RoleName.ROLE_EMPLOYEE.name())).thenReturn(Optional.of(new Role()));

        ResponseEntity<String> response = this.controller.registerEmployeeUser(companyUserSignUpRequestDTO);
        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    void registerManagerUser() {
        CompanyUserSignUpRequestDTO companyUserSignUpRequestDTO = new CompanyUserSignUpRequestDTO();
        companyUserSignUpRequestDTO.setUsername(companyUsername);
        companyUserSignUpRequestDTO.setEmail(email);

        Mockito.when(customerUserRepository.existsByUsername(companyUserSignUpRequestDTO.getUsername())).thenReturn(false);
        Mockito.when(customerUserRepository.existsByEmail(companyUserSignUpRequestDTO.getEmail())).thenReturn(false);
        Mockito.when(roleRepository.findByRoleName(RoleName.ROLE_MANAGER.name())).thenReturn(Optional.of(new Role()));

        ResponseEntity<String> response = this.controller.registerManagerUser(companyUserSignUpRequestDTO);
        assertEquals(200, response.getStatusCodeValue());
    }
}