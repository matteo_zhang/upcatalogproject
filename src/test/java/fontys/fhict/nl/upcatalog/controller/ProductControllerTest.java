package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.model.item.Product;
import fontys.fhict.nl.upcatalog.repository.CategoryRepository;
import fontys.fhict.nl.upcatalog.repository.ProductRepository;
import fontys.fhict.nl.upcatalog.service.ProductService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)

class ProductControllerTest {
    private ProductController productController;
    private MockMvc mvc;

    @InjectMocks
    private ProductService productService;
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private CategoryRepository categoryRepository;

    private final String apiUrl = "/api/product";

    @BeforeEach
    void setup() {
        productController = new ProductController(productService);
        mvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    void getAllProductsTest() throws Exception {
        // When I call findAll() method, mockito will inject these mock data which are empty obj for simplicity
        Mockito.when(productRepository.findAll()).thenReturn(Stream.of(
                new Product(), new Product()
        ).collect(Collectors.toList()));

        mvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", Matchers.hasSize(2)));
    }
}