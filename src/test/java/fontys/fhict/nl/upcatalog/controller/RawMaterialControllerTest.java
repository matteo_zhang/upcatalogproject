package fontys.fhict.nl.upcatalog.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fontys.fhict.nl.upcatalog.repository.*;

import static common.MapToJson.mapToJson;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fontys.fhict.nl.upcatalog.model.item.RawMaterial;
import fontys.fhict.nl.upcatalog.service.RawMaterialService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class RawMaterialControllerTest {
    private MockMvc mvc;
    private RawMaterialController controller;

    @InjectMocks
    private RawMaterialService service;
    @MockBean
    private RawMaterialRepository repository;

    private final String apiUrl = "/api/raw-material";

    @BeforeEach
    void setup() {
        controller = new RawMaterialController(service);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void getAllRawMaterialTest() throws Exception {
        // When I call findAll() method, mockito will inject these mock data which are empty obj for simplicity
        Mockito.when(repository.findAll()).thenReturn(Stream.of(
                new RawMaterial(), new RawMaterial(), new RawMaterial()
        ).collect(Collectors.toList()));

        mvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", Matchers.hasSize(3)));
    }

    @Test
    void addRawMaterialTest() throws Exception {
        RawMaterial raw = new RawMaterial();
        String name = "Dummy raw material";
        raw.setName(name);

        Mockito.when(repository.save(raw)).thenReturn(raw);

        mvc.perform(post(apiUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(mapToJson(raw))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is(name)));
    }

    @Test
    void findRawMaterialTest() throws Exception {
        Long id = 10L;
        String name = "Dummy raw material";

        RawMaterial raw = new RawMaterial();
        raw.setId(id);
        raw.setName(name);
        raw.setRemoved(false);

        Mockito.when(repository.findById(id)).thenReturn(Optional.of(raw));

        mvc.perform(get(apiUrl + "/10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is(name)))
                .andExpect(jsonPath("$.removed", Matchers.is(false)));
    }

    @Test
    void updateRawMaterialTest() throws Exception {
        Long id = 10L;
        String name = "Dummy raw material";
        RawMaterial raw = new RawMaterial();
        raw.setId(id);
        raw.setName(name);

        Mockito.when(repository.findById(id)).thenReturn(Optional.of(raw));
        Mockito.when(repository.save(raw)).thenReturn(raw);

        // update with the new name
        mvc.perform(put(apiUrl + "/10").contentType(MediaType.APPLICATION_JSON)
                    .content(mapToJson(raw)))
            .andExpect(status().isOk());

        // check if returns the new name
        mvc.perform(get(apiUrl + "/10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is(name)));
    }

    @Test
    void removeRawMaterialTest() throws Exception {
        Long id = 10L;
        RawMaterial raw = new RawMaterial();
        raw.setId(id);
        raw.setName("Dummy raw material");
        raw.setRemoved(false);

        Mockito.when(repository.findById(id)).thenReturn(Optional.of(raw));
        Mockito.when(repository.save(raw)).thenReturn(raw);

        mvc.perform(delete(apiUrl + "/10"));

        assertTrue(repository.findById(id).get().isRemoved());
    }

    @Test
    void deleteRawMaterialTest() throws Exception {
        Long id = 10L;
        RawMaterial raw = new RawMaterial();
        raw.setId(id);
        raw.setName("Dummy raw material");
        raw.setRemoved(true); // only previously removed item can be deleted

        Mockito.when(repository.findById(id)).thenReturn(Optional.of(raw));
        Mockito.when(repository.save(raw)).thenReturn(raw);

        mvc.perform(delete(apiUrl + "/delete/" + id));
        // check if the item was deleted
        mvc.perform(get(apiUrl + id))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_Return_BadRequest_when_deleteRawMaterial_With_NoRemovedItem_Test() throws Exception {
        Long id = 10L;
        RawMaterial raw = new RawMaterial();
        raw.setId(id);
        raw.setName("Dummy raw material");
        raw.setRemoved(false); // only previously removed item can be deleted

        Mockito.when(repository.findById(id)).thenReturn(Optional.of(raw));
        Mockito.when(repository.save(raw)).thenReturn(raw);

        mvc.perform(delete(apiUrl + "/delete/" + id)).andExpect(status().is5xxServerError());
    }
}