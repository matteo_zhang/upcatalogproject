package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.dto.CartDTO;
import fontys.fhict.nl.upcatalog.model.CustomerOrder;
import fontys.fhict.nl.upcatalog.model.cart.Cart;
import fontys.fhict.nl.upcatalog.model.cart.ProductCart;
import fontys.fhict.nl.upcatalog.model.item.Product;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import fontys.fhict.nl.upcatalog.repository.CartRepository;
import fontys.fhict.nl.upcatalog.repository.CustomerOrderRepository;
import fontys.fhict.nl.upcatalog.repository.ProductCartRepository;
import fontys.fhict.nl.upcatalog.service.CartService;
import fontys.fhict.nl.upcatalog.service.CustomerOrderService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static common.MapToJson.mapToJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class CustomerCustomerOrderControllerTest {
    private MockMvc mvc;
    private CustomerOrderController controller;

    @InjectMocks
    private CustomerOrderService customerOrderService;
    @MockBean
    private CartService cartService;

    @MockBean
    private CustomerOrderRepository customerOrderRepository;
    @MockBean
    private ProductCartRepository productCartRepository;

    private final String apiUrl = "/api/customer/order";
    private final Long customerId = 10L;
    private CustomerUser customerUser;

    @BeforeEach()
    void setup() {
        controller = new CustomerOrderController(customerOrderService);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();

        customerUser = new CustomerUser();
        customerUser.setId(customerId);
    }

    @Test
    void getOrderList() throws Exception{
        Long productId1 = 1L;
        Long productId2 = 2L;
        int quantity = 10;

        Product p1 = new Product(); p1.setId(productId1); p1.setQuantity(quantity);
        Product p2 = new Product(); p2.setId(productId2); p2.setQuantity(quantity);

        Cart cart = new Cart(customerUser);

        double totalAmount = 1000;
        CustomerOrder order1 = new CustomerOrder(cart, customerUser, totalAmount);
        CustomerOrder order2 = new CustomerOrder(cart, customerUser, totalAmount);

        Mockito.when(customerOrderRepository.findAll()).thenReturn(Stream.of(
            order1, order2
        ).collect(Collectors.toList()));
        Mockito.when(cartService.getEnabledCartList(customerId)).thenReturn(Stream.of(
                new CartDTO(customerUser, quantity, p1), new CartDTO(customerUser, quantity, p2)
        ).collect(Collectors.toList()));

        mvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", Matchers.hasSize(2)));
    }

    @Test
    void generateOrder() throws Exception {
        Long productId1 = 1L;
        Long productId2 = 2L;
        int quantity = 10;

        Product p1 = new Product(); p1.setId(productId1); p1.setQuantity(quantity);
        Product p2 = new Product(); p2.setId(productId2); p2.setQuantity(quantity);

        Mockito.when(cartService.getEnabledCartList(customerId)).thenReturn(Stream.of(
                new CartDTO(customerUser, quantity, p1), new CartDTO(customerUser, quantity, p2)
        ).collect(Collectors.toList()));
        Mockito.when(cartService.getEnabledCart(customerId)).thenReturn(new Cart(customerUser));
        Mockito.when(customerOrderRepository.save(new CustomerOrder())).thenReturn(new CustomerOrder());
        Mockito.when(cartService.disableCurrentlyUsingCart(customerId)).thenReturn(true);

        mvc.perform(put(apiUrl)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Objects.requireNonNull(mapToJson(customerId))))
                .andExpect(status().isOk());

    }
}