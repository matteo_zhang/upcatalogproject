package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.dto.CustomerNotificationDTO;
import fontys.fhict.nl.upcatalog.model.notification.CustomerNotification;
import fontys.fhict.nl.upcatalog.model.notification.NotificationType;
import fontys.fhict.nl.upcatalog.repository.CustomerNotificationRepository;
import fontys.fhict.nl.upcatalog.repository.RawMaterialRepository;
import fontys.fhict.nl.upcatalog.service.CustomerNotificationService;
import fontys.fhict.nl.upcatalog.service.RawMaterialService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static common.MapToJson.mapToJson;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class CustomerNotificationControllerTest {
    private MockMvc mvc;
    private CustomerNotificationController controller;

    @InjectMocks
    private CustomerNotificationService service;
    @MockBean
    private CustomerNotificationRepository repository;

    private final String apiUrl = "/api/customer/notification";

    @BeforeEach
    void setup() {
        controller = new CustomerNotificationController(service);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void getAllCustomerNotificationTest() throws Exception {
        Mockito.when(repository.findAll()).thenReturn(Stream.of(
                new CustomerNotification(), new CustomerNotification(), new CustomerNotification()
        ).collect(Collectors.toList()));

        mvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", Matchers.hasSize(3)));
    }

    @Test
    void addCustomerNotificationTest() throws Exception {
        String username = "jackobs";
        String customerEmail = "my@gmail.com";

        // send the DTO of CustomerNotification
        CustomerNotificationDTO dto = new CustomerNotificationDTO();
        dto.setUsername(username);
        dto.setCustomerEmail(customerEmail);

        CustomerNotification notification = new CustomerNotification();
        notification.setUsername(username);
        notification.setCustomerEmail(customerEmail);

        Mockito.when(repository.save(notification)).thenReturn(notification);

        mvc.perform(post(apiUrl)
                .content(Objects.requireNonNull(mapToJson(dto)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.username", Matchers.is(username)));
    }
}