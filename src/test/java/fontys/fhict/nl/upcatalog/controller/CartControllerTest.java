package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.dto.CartDTO;
import fontys.fhict.nl.upcatalog.model.cart.Cart;
import fontys.fhict.nl.upcatalog.model.cart.ProductCart;
import fontys.fhict.nl.upcatalog.model.item.Product;
import fontys.fhict.nl.upcatalog.model.user.CustomerUser;
import fontys.fhict.nl.upcatalog.repository.CartRepository;
import fontys.fhict.nl.upcatalog.repository.ProductCartRepository;
import fontys.fhict.nl.upcatalog.repository.ProductRepository;
import fontys.fhict.nl.upcatalog.service.CartService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static common.MapToJson.mapToJson;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class CartControllerTest {
    private MockMvc mvc;
    private CartController controller;

    @InjectMocks
    private CartService service;
    @MockBean
    private CartRepository cartRepository;
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private ProductCartRepository productCartRepository;

    private final String apiUrl = "/api/cart";
    private final Long customerId = 10L;
    private final Long cartId = 20L;

    @BeforeEach
    public void setup() {
        controller = new CartController(service);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();

        Mockito.when(cartRepository.getLastEnabledCartId(customerId)).thenReturn(cartId);
    }

    @Test
    void getAllEnabledCartList() throws Exception {
        Long productId1 = 1L;
        Long productId2 = 2L;
        int quantity = 10;

        Product p1 = new Product(); p1.setId(productId1); p1.setQuantity(quantity);
        Product p2 = new Product(); p2.setId(productId2); p2.setQuantity(quantity);
        ProductCart pc1 = new ProductCart(); pc1.setId(productId1); pc1.setQuantity(quantity); pc1.setProduct(p1);
        ProductCart pc2 = new ProductCart(); pc2.setId(productId2); pc2.setQuantity(quantity); pc2.setProduct(p2);

        Mockito.when(productRepository.getAllProductsFromEnabledCart(cartId)).thenReturn(Stream.of(
                p1, p2
        ).collect(Collectors.toList()));
        Mockito.when(productCartRepository.getQuantities(cartId)).thenReturn(Stream.of(
                pc1, pc2
        ).collect(Collectors.toList()));

        mvc.    perform(get(apiUrl + "/10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", Matchers.hasSize(2)));
    }

    @Test
    void addCartElement_When_IsNewCartInstance() throws Exception {
        CustomerUser user = new CustomerUser();
        user.setId(customerId);
        int quantity = 4;
        Product product = new Product();
        CartDTO dto = new CartDTO(user, quantity, product);

        Cart newCart = new Cart(dto.getCustomerUser());
        ProductCart prCart = new ProductCart(dto.getProduct(), newCart, dto.getQuantity());

        // there is not a using cart instance, so create a new one
        Mockito.when(cartRepository.getLastEnabledCartId(customerId)).thenReturn(null);
        Mockito.when(cartRepository.save(newCart)).thenReturn(newCart);
        Mockito.when(productCartRepository.save(prCart)).thenReturn(prCart);

        mvc.perform(post(apiUrl)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(Objects.requireNonNull(mapToJson(dto))))
                .andExpect(status().isOk());
    }

    @Test
    void addCartElement_When_ExistsCartInstance() throws Exception {
        CustomerUser user = new CustomerUser();
        user.setId(customerId);
        int quantity = 4;
        Product product = new Product();
        CartDTO dto = new CartDTO(user, quantity, product);

        Cart foundCart = new Cart(dto.getCustomerUser());
        ProductCart prCart = new ProductCart(dto.getProduct(), foundCart, dto.getQuantity());

        // a cart instance is currently using
        Mockito.when(cartRepository.getLastEnabledCartId(customerId)).thenReturn(cartId);
        Mockito.when(cartRepository.findById(cartId)).thenReturn(Optional.of(foundCart));
        Mockito.when(productCartRepository.save(prCart)).thenReturn(prCart);

        mvc.perform(post(apiUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(mapToJson(dto))))
                .andExpect(status().isOk());
    }

    @Test
    void changeCartElement() throws Exception {
        CustomerUser user = new CustomerUser();
        user.setId(customerId);
        int quantity = 4;
        Product product = new Product();
        CartDTO dto = new CartDTO(user, quantity, product);

        Cart foundCart = new Cart(dto.getCustomerUser());
        ProductCart prCart = new ProductCart(dto.getProduct(), foundCart, dto.getQuantity());

        Mockito.when(cartRepository.findById(cartId)).thenReturn(Optional.of(foundCart));
        Mockito.when(productCartRepository.findByCartId(foundCart.getId(), dto.getProduct().getId())).thenReturn(Optional.of(prCart));
        Mockito.when(productCartRepository.save(prCart)).thenReturn(prCart);

        mvc.perform(put(apiUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(mapToJson(dto))))
                .andExpect(status().isOk());
    }
}