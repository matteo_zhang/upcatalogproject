package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.model.user.CompanyUser;
import fontys.fhict.nl.upcatalog.repository.CompanyUserRepository;
import fontys.fhict.nl.upcatalog.repository.RoleRepository;
import fontys.fhict.nl.upcatalog.service.CompanyUserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class CompanyUserControllerTest {
    private MockMvc mockMvc;
    private CompanyUserController companyUserController;

    @InjectMocks
    private CompanyUserService companyUserService;
    @MockBean
    private CompanyUserRepository companyUserRepository;

    @MockBean
    RoleRepository roleRepository;

    private final String apiUrl = "/api/company";

    @BeforeEach
    public void setup() {
        companyUserController = new CompanyUserController(companyUserService);
        mockMvc = MockMvcBuilders.standaloneSetup(companyUserController).build();
    }

    @Test
    void getAllCompanyUsersTest() throws Exception {
        Mockito.when(companyUserRepository.findAll()).thenReturn(Stream.of(
                new CompanyUser(), new CompanyUser(), new CompanyUser()
        ).collect(Collectors.toList()));

        mockMvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", Matchers.hasSize(3)));
    }

    @Test
    void findCompanyUsersByIdTest() throws Exception {
        CompanyUser user = new CompanyUser();
        Long id = 10L;
        String username = "jassdry";
        user.setId(id);
        user.setUsername(username);

        Mockito.when(companyUserRepository.findById(id)).thenReturn(Optional.of(user));

        mockMvc.perform(get(apiUrl + "/10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", Matchers.is(username)));
    }
}