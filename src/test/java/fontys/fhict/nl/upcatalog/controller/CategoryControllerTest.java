package fontys.fhict.nl.upcatalog.controller;

import fontys.fhict.nl.upcatalog.model.item.Category;
import fontys.fhict.nl.upcatalog.repository.*;
import fontys.fhict.nl.upcatalog.service.CategoryService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class CategoryControllerTest {
    private MockMvc mockMvc;
    private CategoryController categoryController;

    @InjectMocks
    CategoryService service;

    @MockBean
    private CategoryRepository categoryRepository;

    private final String apiUrl = "/api/category";

    @BeforeEach
    public void setup() {
        categoryController = new CategoryController(service);
        mockMvc = MockMvcBuilders.standaloneSetup(categoryController).build();
    }



    @Test
    void getAllProductsTest() throws Exception {
        Mockito.when(categoryRepository.findAll()).thenReturn(Stream.of(
                new Category(), new Category(), new Category()
        ).collect(Collectors.toList()));

        mockMvc.perform(get(apiUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*]", Matchers.hasSize(3)));
    }
}